//
//  MessageTableViewCell.swift

import UIKit

class MessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var zenChatMessageID: UILabel!
    @IBOutlet weak var zenChatUserNameLabel: UILabel!
    @IBOutlet weak var zenChatUserID: UILabel!
    @IBOutlet weak var zenChatMessageTextView: UITextView!
    
    //@IBOutlet weak var zenChatMessageSentTime: UILabel!
    @IBAction func unwindToMessageTableViewCell(segue: UIStoryboardSegue) {
        print("Returned from detail screen")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
