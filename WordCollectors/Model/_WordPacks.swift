// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to WordPacks.swift instead.

import CoreData

public enum WordPacksAttributes: String {
    case packCost = "packCost"
    case packID = "packID"
    case packName = "packName"
    case purchased = "purchased"
    case userID = "userID"
    case words = "words"
}

public enum WordPacksRelationships: String {
    case user = "user"
}

@objc public
class _WordPacks: NSManagedObject {

    // MARK: - Class methods

    public class func entityName () -> String {
        return "WordPacks"
    }

    public class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    public convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _WordPacks.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged public
    var packCost: NSNumber?

    // func validatePackCost(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var packID: NSNumber?

    // func validatePackID(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var packName: String?

    // func validatePackName(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var purchased: NSNumber?

    // func validatePurchased(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var userID: NSNumber?

    // func validateUserID(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var words: String?

    // func validateWords(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    // MARK: - Relationships

    @NSManaged public
    var user: User?

    // func validateUser(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

}

