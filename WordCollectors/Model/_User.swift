// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to User.swift instead.

import CoreData

public enum UserAttributes: String {
    case firstName = "firstName"
    case lastName = "lastName"
    case money = "money"
    case userID = "userID"
    case userName = "userName"
    case wordsLeft = "wordsLeft"
}

public enum UserRelationships: String {
    case messages = "messages"
    case wordpack = "wordpack"
}

@objc public
class _User: NSManagedObject {

    // MARK: - Class methods

    public class func entityName () -> String {
        return "User"
    }

    public class func entity(managedObjectContext: NSManagedObjectContext!) -> NSEntityDescription! {
        return NSEntityDescription.entityForName(self.entityName(), inManagedObjectContext: managedObjectContext);
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }

    public convenience init(managedObjectContext: NSManagedObjectContext!) {
        let entity = _User.entity(managedObjectContext)
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged public
    var firstName: String?

    // func validateFirstName(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var lastName: String?

    // func validateLastName(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var money: NSNumber?

    // func validateMoney(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var userID: NSNumber?

    // func validateUserID(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var userName: String?

    // func validateUserName(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    @NSManaged public
    var wordsLeft: NSNumber?

    // func validateWordsLeft(value: AutoreleasingUnsafeMutablePointer<AnyObject>, error: NSErrorPointer) -> Bool {}

    // MARK: - Relationships

    @NSManaged public
    var messages: NSSet

    @NSManaged public
    var wordpack: NSSet

}

extension _User {

    func addMessages(objects: NSSet) {
        let mutable = self.messages.mutableCopy() as! NSMutableSet
        mutable.unionSet(objects as! Set<NSObject>)
        self.messages = mutable.copy() as! NSSet
    }

    func removeMessages(objects: NSSet) {
        let mutable = self.messages.mutableCopy() as! NSMutableSet
        mutable.minusSet(objects as! Set<NSObject>)
        self.messages = mutable.copy() as! NSSet
    }

    func addMessagesObject(value: Message!) {
        let mutable = self.messages.mutableCopy() as! NSMutableSet
        mutable.addObject(value)
        self.messages = mutable.copy() as! NSSet
    }

    func removeMessagesObject(value: Message!) {
        let mutable = self.messages.mutableCopy() as! NSMutableSet
        mutable.removeObject(value)
        self.messages = mutable.copy() as! NSSet
    }

}

extension _User {

    func addWordpack(objects: NSSet) {
        let mutable = self.wordpack.mutableCopy() as! NSMutableSet
        mutable.unionSet(objects as! Set<NSObject>)
        self.wordpack = mutable.copy() as! NSSet
    }

    func removeWordpack(objects: NSSet) {
        let mutable = self.wordpack.mutableCopy() as! NSMutableSet
        mutable.minusSet(objects as! Set<NSObject>)
        self.wordpack = mutable.copy() as! NSSet
    }

    func addWordpackObject(value: WordPacks!) {
        let mutable = self.wordpack.mutableCopy() as! NSMutableSet
        mutable.addObject(value)
        self.wordpack = mutable.copy() as! NSSet
    }

    func removeWordpackObject(value: WordPacks!) {
        let mutable = self.wordpack.mutableCopy() as! NSMutableSet
        mutable.removeObject(value)
        self.wordpack = mutable.copy() as! NSSet
    }

}

