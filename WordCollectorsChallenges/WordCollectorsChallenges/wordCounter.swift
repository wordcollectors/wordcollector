//
//  wordCounter.swift
//  WordCollectorsChallenges
//
//  Created by Huan-Hua Chye on 2/27/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation

class wordCounter {
    
    func count(startingCount: Int, message: [String], purchasedWords: [String]) -> Int {
        
        var messageMinusPurchased: [String] = []
        
        //force all to lowercase to allow case-insensitive compare
        for word in message {
            messageMinusPurchased.append(word.localizedLowercaseString)
        }
        
        if purchasedWords.count > 0 {
            for purchasedWord in purchasedWords {
                if let foundWord = messageMinusPurchased.indexOf(purchasedWord.localizedLowercaseString) {
                    //if lower-case version of the purchased word is found in the message, remove it from the copy of hte message string so it won't be counted and subtracted
                    messageMinusPurchased.removeAtIndex(foundWord)
                    print("removed \(foundWord); new string is \(messageMinusPurchased)")
                }
            }
        }
        return startingCount - messageMinusPurchased.count
    }

}