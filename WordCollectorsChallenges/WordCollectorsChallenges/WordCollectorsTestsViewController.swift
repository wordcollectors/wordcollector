//
//  WordCollectorsTestsViewController.swift
//  WordCollectorsChallenges
//
//  Created by Huan-Hua Chye on 2/22/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit

class WordCollectorsTestsViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    var currentWordCount: Int  = 100
    
    @IBOutlet weak var messageField: UITextField!
    
    @IBOutlet weak var methodPicker: UIPickerView!
    
    @IBOutlet weak var resultsField: UITextField!
    
    @IBOutlet weak var wordCount: UIBarButtonItem!
    
    @IBAction func runTestButtonPressed(sender: AnyObject) {
        let splitter = Splitter()
        let purchasedWords = splitter.split("Dog cat bird")
        
        let input = splitter.split(messageField.text ?? "")
        switch methodPicker.selectedRowInComponent(0) {
        case 0:
            let lengthTest = LengthLimit(limitOf:10)
            resultsField.text = String(lengthTest.test(input))
        case 1:
            let forbiddenChars = ForbiddenCharacters(DNU: "E")
            resultsField.text = String(forbiddenChars.testDNU(input))
        case 2:
            let startsWith = StartsWith(firstLetter: "A")
            resultsField.text = String(startsWith.test(input))
        case 3:
            let endsWith = EndsWith(lastLetter: "S")
            resultsField.text = String(endsWith.test(input))
        default:
            print("None of the above")
        }
        let wc = wordCounter()
        currentWordCount = wc.count(currentWordCount, message: input, purchasedWords: purchasedWords)
        wordCount.title = String("\(currentWordCount) words")
        
        let wcFilter = WordCollectorsMessageFilter(key: "asdf!!!")
        let mcf = messageField.text
        let mcfEncoded = wcFilter.encodeWCMessage(mcf ?? "")
        print("encoded message: \(mcfEncoded)")
        if wcFilter.isMessageWCCompliant(mcfEncoded) {
            print(wcFilter.decodeWCMessage(mcfEncoded))
        }
        
    }
    
    var pickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        self.methodPicker.dataSource = self
        self.methodPicker.delegate = self
        pickerData = ["Length Limit: 10","Forbidden Characters: E","Starts With: A","Ends With: S"]
        wordCount.title = "\(currentWordCount) words"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // The number of columns of data
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }


    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
