//
//  LengthLimit.swift
//  WordCollectorsChallenges
//
//  Created by Huan-Hua Chye on 2/15/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation

class LengthLimit {
    
    var limitOf: Int
    
    //init with limitOf to give it the character limit you want to enforce
    
    init(limitOf: Int) {
        self.limitOf = limitOf
    }
    
    //LengthLimit.test(message) 
    
    func test(message: [String]) -> Bool {
        var longerThanLimit = false
        
        for word in message {
//            print(word.characters.count)
            if word.characters.count > limitOf {
                longerThanLimit = true
//                print("setting longerThanLimit to true")
            }
        }
        return longerThanLimit
    }
    
    
    
}