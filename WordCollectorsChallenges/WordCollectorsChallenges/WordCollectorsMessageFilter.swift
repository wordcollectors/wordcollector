//
//  WordCollectorsMessageFilter.swift
//  WordCollectorsChallenges
//
//  Created by Huan-Hua Chye on 2/28/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation

//The idea is to add a special key onto Fireside strings and only accept strings with this key
//Contains utilities to test whether message belongs to WordCollectors, add the key before sending to Fireside, and remove the key from WC messages

class WordCollectorsMessageFilter {
    var key: String
    
    init(key: String) {
        self.key = key
    }
    
    func isMessageWCCompliant(messageString: String) -> Bool {
        if messageString.containsString(key) {
            return true
        } else {
            return false
        }
        
    }
    
    func encodeWCMessage(messageString: String) -> String {
        //Run this to add key to messages before sending them to the server
        var encodedString = messageString
        encodedString.appendContentsOf(key)
        return encodedString
    }
    
    func decodeWCMessage(messageString: String) -> String {
        //Run this to remove key from messages before splitting them
        var decodedString = messageString
        decodedString.removeRange(decodedString.rangeOfString(key)!)
        return decodedString
    }
    
}