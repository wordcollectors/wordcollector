//
//  Splitter.swift
//  WordCollectorsChallenges
//
//  Created by Huan-Hua Chye on 2/15/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation

class Splitter {
    //Splits up a string on whitespace and returns an array of words in the string
    
    func split(messageString: String)->[String] {
        let whitespaceCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
        let components = messageString.componentsSeparatedByCharactersInSet(whitespaceCharacterSet)
        return components
    }
}