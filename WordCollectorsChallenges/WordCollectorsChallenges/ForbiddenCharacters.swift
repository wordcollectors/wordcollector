//
//  ForbiddenCharacters.swift
//  WordCollectorsChallenges
//
//  Created by Huan-Hua Chye on 2/22/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation

class ForbiddenCharacters {
 
    var DNU: Character
    
    //init with DNU to give it the character that should not be used
    
    init(DNU: Character) {
        self.DNU = DNU
    }
    
    //ForbiddenCharacters.test(message)--case-insensitive
    
    func testDNU(message: [String]) -> Bool {
        var usesDNU = false
        
        for word in message {
            for char in word.characters {
                if String(char).localizedCaseInsensitiveContainsString(String(DNU)) {
                    usesDNU = true
                }
            }
        }
        return usesDNU
    }

    
}