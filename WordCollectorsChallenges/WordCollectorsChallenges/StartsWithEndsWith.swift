//
//  StartsWith.swift
//  WordCollectorsChallenges
//
//  Created by Huan-Hua Chye on 2/28/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import Foundation

class StartsWith {
    
    var firstLetter: Character
    
    //return true if it starts with the character
    //case-insensitive
    
    init(firstLetter: Character) {
        self.firstLetter = Character(String(firstLetter).localizedLowercaseString)
    }
    

    func test(message: [String]) -> Bool {
        var startsWithLetter = false
        
        for word in message {
//            print("self.firstLetter: \(self.firstLetter); first letter: \(word.characters.first!)")
            if String(self.firstLetter) == String(word.characters.first!).localizedLowercaseString {
              startsWithLetter = true
            }
        }
    
    return startsWithLetter
    }
    
}

class EndsWith {
    
    var lastLetter: Character
    
    //return true if it ends with the character
    //case-insensitive
    init(lastLetter: Character) {
        self.lastLetter = Character(String(lastLetter).localizedLowercaseString)
    }
    
    func test(message: [String]) -> Bool {
        var endsWithLetter = false
        
        for word in message {
//            print("self.lastLetter: \(self.lastLetter); last letter: \(word.characters.last!)")
            if String(self.lastLetter) == String(word.characters.last!).localizedLowercaseString {
                endsWithLetter = true
            }
        }
        
        return endsWithLetter
    }
    
}