//
//  ViewController.swift
//  UIPickerView
//
//  Created by Justin Janisch on 2/22/16.
//  Copyright © 2016 Justin Janisch. All rights reserved.
// 
//  This tutorial was developed using the UIPickerview Tutorial v2 on www.makeapppie.com/tag/uipickerview-in-swift/ as a template.
//  Swift 2.0  iOS 9.0
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var wordPackage: UIPickerView!
    @IBOutlet weak var labelConfirm: UILabel!
    
    // Test Data for Project
    let pickerData = ["Deadly Animals", "Famous Baby Names", "Pod Vegetables", "Movie Cars", "Klingon Phrases", "Sports Injuries", "Inconspicuous Holidays", "3 Letter Baby Names", "Roads in Maine", "Insect Anatomy", "Board Game Pieces", "Broadway Musical Titles", "Units of Measure", "TV catchphrases", "Types of Waves", "Don't get bitten by these", "Footwear", "Palabras en Espanol", "60's & 70's Song Titles", "Cooking Utensils"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wordPackage.dataSource = self
        wordPackage.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Custom Labels for the pickerview.  Below will modify Font, Font Colors, Background Colors, and Alignment
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        let titleData = pickerData[row]
        let hue = CGFloat(row) / CGFloat(pickerData.count)
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Didot", size: 18.0)!,NSForegroundColorAttributeName:UIColor.blackColor()])
        
        pickerLabel.backgroundColor = UIColor(hue: hue, saturation: 1.0, brightness: 1.0, alpha: 1.0)
        pickerLabel.textAlignment = .Center
        pickerLabel.attributedText = myTitle
        return pickerLabel
    }


    // MARK: - Deligates & Datasources
    
    // Picker item selected
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    // Set label on View to Picker Selection
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        labelConfirm.text = pickerData[row]
    }
    
    // Modify this if you want to add segmented/multi pickerview areas
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Sets the number of rows in the UIPickerView
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // MARK: - Sizing the Label
    
    // Cell/Label Height
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 36.0
    }
    
    // Label Width
    func pickerView(pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 230
    }
}

