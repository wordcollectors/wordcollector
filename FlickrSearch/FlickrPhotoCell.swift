//
//  FlickrPhotoCell.swift
//  FlickrSearch
//
//  Created by Huan-Hua Chye on 1/31/16.
//  Copyright © 2016 Huan-Hua Chye. All rights reserved.
//

import UIKit

class FlickrPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
