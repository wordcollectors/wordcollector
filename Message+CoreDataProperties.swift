//
//  Message+CoreDataProperties.swift
//  Firehouse
//
//  Created by Justin M Janisch on 12/8/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Message {

    @NSManaged var messageBody: String?
    @NSManaged var messageID: NSNumber?
    @NSManaged var messageType: String?
    @NSManaged var userID: NSNumber?
    @NSManaged var userFirstName: String?
    @NSManaged var userLastName: String?
    @NSManaged var userName: String?
    @NSManaged var user: User?

}
