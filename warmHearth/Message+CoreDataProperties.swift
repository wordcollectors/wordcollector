//
//  Message+CoreDataProperties.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/11/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Message {

    @NSManaged var messageID: NSNumber?
    @NSManaged var userID: NSNumber?
    @NSManaged var messageBody: String?
    @NSManaged var messageType: String?
    @NSManaged var userFirstName: String?
    @NSManaged var userLastName: String?
    @NSManaged var userName: String?
    @NSManaged var user: User?

}
