//
//  MessagesTableViewController.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/9/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import UIKit

class MessagesTableViewController: UITableViewController {
//MARK: properties
    var messagesDataSource : MessagesDataSource?

    //MARK: Outlets
    
//MARK: Actions

    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        tableView.reloadData()
    }

    @IBAction func logOutButtonTapped(sender: UIBarButtonItem) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setValue("", forKey: "token")
        userDefaults.setBool(false, forKey: "isLoggedIn")
        print("logging out--isLoggedIn is \(userDefaults.boolForKey("isLoggedIn"))")
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //MARK: methods
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        
        messagesDataSource = MessagesDataSource()
        messagesDataSource!.managedObjectContext = managedObjectContext
        messagesDataSource?.tableView = tableView
        tableView.dataSource = messagesDataSource!

        self.refreshControl?.addTarget(self, action: "handleRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableView.reloadData()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    func handleRefresh(refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
  
        messagesDataSource?.retrieveMessages({ () -> Void in
            self.refreshControl?.endRefreshing()
        })

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "messageDetailViewSegue" {

            let senderCell = sender as! MessageTableViewCell

            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setObject(senderCell.userNameLabel.text ?? "[username not available]", forKey: "currentMessageUserName")
            userDefaults.setObject(senderCell.fullNameLabel.text ?? "[full name not available]", forKey: "currentMessageFullName")
            userDefaults.setObject(senderCell.messageTextLabel.text ?? "[message body not available]", forKey: "currentMessageText")
     
        }
    }
    

    
    
    
}
