//
//  loginScreenTableViewController.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/8/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import UIKit

class loginScreenTableViewController: UITableViewController {
    //MARK: properties
//    var loginAuthenticated: Bool = false
//    var mySettings = Settings()

    //MARK: Outlets
    
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var errorLabel: UILabel!
    //MARK: Actions
    
    
    @IBAction func loginButtonTapped(sender: UIButton) {
        
        authenticateLogin(userNameField.text!, password: passwordField.text!)
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if userDefaults.boolForKey("isLoggedIn") == true {
            showTabScreen()
        }
    }
    
    func showTabScreen() {
//        let tabController : AnyObject! = self.storyboard?.instantiateViewControllerWithIdentifier("tabBarViewController")
//        presentViewController(tabController as! UIViewController, animated: true, completion: nil)
        performSegueWithIdentifier("showTabControllerFromLogin", sender: self)
        
    }
    
    //MARK: Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if userDefaults.boolForKey("isLoggedIn") {
            showTabScreen()
        }
        userNameField.addTarget(self, action: "validateLogins:", forControlEvents: .EditingChanged)
        passwordField.addTarget(self, action: "validateLogins:", forControlEvents: .EditingChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: functions
    
    func validateLogins(sender: UITextField) {
        //Validate that the format is correct
        
        let userNameText = userNameField.text ?? ""
        let passwordText = passwordField.text ?? ""
        let userFieldEntered = userNameText.characters.count > 4
        let passwordFieldEntered = passwordText.characters.count >= 10
        
        let loginTextValid = userFieldEntered && passwordFieldEntered
//        errorLabel.text = ""
        
        if loginTextValid {
            loginButton.enabled = true
        } else {
            loginButton.enabled = false
        }
        }
        


    func authenticateLogin(userName: String, password: String) -> Bool {
 
        //make a dictionary of the username/password
        let userNameDictionary = ["userName": userName, "password": password]
        print(userNameDictionary)
        
        //optionals to hold data we will be creating
        var jsonData: NSData?
        var jsonString: NSString?
        var token: String?
        
        //create the JSON data object
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(userNameDictionary, options: [])
        } catch {
            print("Error creating JSON")
        }
        
        //create the request and session objects
        let request = NSMutableURLRequest(URL: NSURL(string: "http://198.150.10.30:8080/fireside/login")!)
        let session = NSURLSession.sharedSession()
        
        //set the headers
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData!
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
       // print("4")
        
        //first line of the closure
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            //unwrap and cast the response
         //   print("5")
            guard let testResponse = response as? NSHTTPURLResponse else {
                print("Invalid Response from server: \(response)")
                dispatch_sync(dispatch_get_main_queue()) {
                    self.errorLabel.text = "Unable to log in"
                }
                return
                
            }
            
           // print("6")
            
            //extract the status code
            
            guard let status = HTTPStatusCodes(rawValue: testResponse.statusCode) else {
                print("A really strange thing happened.")
                dispatch_sync(dispatch_get_main_queue()) {
                    self.errorLabel.text = "Unable to log in"
                }
                return
            }
            
            //print("7")
            
            //switch on the status
            
            switch status {
            case .Created:
                //print("8")
                let headerFields = testResponse.allHeaderFields
                token = headerFields["user-auth-token"] as? String
                let userDefaults = NSUserDefaults.standardUserDefaults()
                if token != nil {
                    userDefaults.setBool(true, forKey: "isLoggedIn")
                    print("setting isLoggedIn to true")
//                    print("The token is \(token). Login Authentication = \(self.loginAuthenticated)")
//                    self.showTabScreen()
//                    print("Let's go!")
                    userDefaults.setObject(token!, forKey: "token")
                }
                
                guard let returnedData = data else {
                    print("no data was returned")
                    dispatch_sync(dispatch_get_main_queue()) {
                        self.errorLabel.text = "Unable to log in"
                    }
                    break
                }
                jsonString = NSString(data: returnedData, encoding: NSUTF8StringEncoding) ?? "Invalid JSON"
                print("\(token!)\n\n\(jsonString!)")
                dispatch_sync(dispatch_get_main_queue()) {
                    self.showTabScreen()
                    print(token)
                }
            default:
                
                print("Unknown status: \(status.rawValue)")
               dispatch_sync(dispatch_get_main_queue()) {
                self.errorLabel.text = "Unable to log in"
                }
            }
            
        }
        task.resume()
        return true
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if let dest = segue.destinationViewController as? UINavigationController {
//            print("passing along \(mySettings.token)")
//            if let destSettingsController = dest.topViewController as? settingsControllerTableViewController {
//                destSettingsController.mySettings = self.mySettings
//                destSettingsController.tableView.reloadData()
//            }
//        }
    }
    

}
