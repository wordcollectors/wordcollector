//
//  newMessageViewController.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/11/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import UIKit

class newMessageViewController: UIViewController {
    //MARK: Properties
    
//MARK: Outlets
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var messageText: UITextView!

    @IBOutlet var newMessageViewController: UIView!
    //MARK: Actions
    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func saveButtonTapped(sender: AnyObject) {
     
//        currentMessage.text = messageText.text

        //make a dictionary of the message body/message type
            let messageDictionary = ["messageBody": messageText.text, "messageType": "public"]
            print(messageDictionary)
            
            //optionals to hold data we will be creating
            var jsonData: NSData?
            var jsonString: NSString?
        
            //create the JSON data object
            do {
                jsonData = try NSJSONSerialization.dataWithJSONObject(messageDictionary, options: [])
            } catch {
                print("Error creating JSON")
            }
            
            //create the request and session objects
            let request = NSMutableURLRequest(URL: NSURL(string: "http://198.150.10.30:8080/fireside/messages")!)
            let session = NSURLSession.sharedSession()
            
            //set the headers
            request.HTTPMethod = "POST"
            request.HTTPBody = jsonData!
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let userDefaults = NSUserDefaults.standardUserDefaults()
        request.addValue(userDefaults.objectForKey("token") as! String, forHTTPHeaderField: "user-auth-token")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            // print("4")
            
            //first line of the closure
            let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
                //unwrap and cast the response
                //   print("5")
                guard let testResponse = response as? NSHTTPURLResponse else {
                    print("Invalid Response from server: \(response)")
                    return
                    
                }
                
                // print("6")
                
                //extract the status code
                
                guard let status = HTTPStatusCodes(rawValue: testResponse.statusCode) else {
                    print("A really strange thing happened.")
                    return
                }
                
                //print("7")
                
                //switch on the status
                
                switch status {
                case .Created:
                    //print("8")
//                    let headerFields = testResponse.allHeaderFields
//                    let messageID = headerFields["messageID"] as? Int
//                    if let messageID = messageID {
//                        self.currentMessage.messageID = messageID
//                    }
                    
                    guard let returnedData = data else {
                        print("no data was returned")
                        break
                    }
                    jsonString = NSString(data: returnedData, encoding: NSUTF8StringEncoding) ?? "Invalid JSON"
                    print(jsonString)
                    dispatch_sync(dispatch_get_main_queue()) {
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                default:
                    print("Unknown status: \(status.rawValue)")
                }
                
            }
            task.resume()
    }
    

    

    //MARK: Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
    
        messageText.font = UIFont.systemFontOfSize(CGFloat(userDefaults.floatForKey("fontSize")))

        if userDefaults.boolForKey("backgroundIsBlack") {
            messageText.backgroundColor = UIColor.blackColor()
            messageText.textColor = UIColor.whiteColor()
            newMessageViewController.backgroundColor = UIColor.blackColor()
            
        } else if !userDefaults.boolForKey("backgroundIsBlack") {
            messageText.backgroundColor = UIColor.whiteColor()
            messageText.textColor = UIColor.blackColor()
            newMessageViewController.backgroundColor = UIColor.whiteColor()
        }

    
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
