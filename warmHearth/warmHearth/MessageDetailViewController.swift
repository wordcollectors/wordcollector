//
//  MessageDetailViewController.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/14/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import UIKit

class MessageDetailViewController: UIViewController {

    //MARK: Outlets
    
    @IBOutlet weak var messageDetailTextView: UITextView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var fullNameLabel: UILabel!
//MARK: Actions
    @IBAction func doneButtonTapped(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    //MARK: methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userNameLabel.text = userDefaults.objectForKey("currentMessageUserName") as? String
        fullNameLabel.text = userDefaults.objectForKey("currentMessageFullName") as? String
        messageDetailTextView.text = userDefaults.objectForKey("currentMessageText") as? String 
        userNameLabel.font = UIFont.systemFontOfSize(CGFloat(userDefaults.floatForKey("fontSize")))
        fullNameLabel.font = UIFont.systemFontOfSize(CGFloat(userDefaults.floatForKey("fontSize")))
        messageDetailTextView.font = UIFont.systemFontOfSize(CGFloat(userDefaults.floatForKey("fontSize")))
        
        if userDefaults.boolForKey("backgroundIsBlack") {
            
            self.view.backgroundColor = UIColor.blackColor()
            
            userNameLabel.textColor = UIColor.whiteColor()
            fullNameLabel.textColor = UIColor.whiteColor()
            messageDetailTextView.textColor = UIColor.whiteColor()
            
        } else if !userDefaults.boolForKey("backgroundIsBlack") {
            self.view.backgroundColor = UIColor.whiteColor()
            
            userNameLabel.textColor = UIColor.blackColor()
            fullNameLabel.textColor = UIColor.blackColor()
            messageDetailTextView.textColor = UIColor.blackColor()
            
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
