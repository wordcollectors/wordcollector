//
//  settingsControllerTableViewController.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/9/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import UIKit

class settingsControllerTableViewController: UITableViewController {
    
    //MARK: properties
    
    //MARK: Outlets
    
    @IBOutlet weak var smallCell: UITableViewCell!
    
    @IBOutlet weak var mediumCell: UITableViewCell!
    
    @IBOutlet weak var largeCell: UITableViewCell!
    
    @IBOutlet weak var blackBackgroundCell: UITableViewCell!
    
    @IBOutlet weak var whiteBackgroundCell: UITableViewCell!
    

    
    //MARK: methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        smallCell.accessoryType = .None
        mediumCell.accessoryType = .None
        largeCell.accessoryType = .None
        blackBackgroundCell.accessoryType = .None
        whiteBackgroundCell.accessoryType = .None
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        switch userDefaults.floatForKey("fontSize") {
        case 13 : smallCell.accessoryType = .Checkmark
        case 17 : mediumCell.accessoryType = .Checkmark
        case 23 : largeCell.accessoryType = .Checkmark
        default: mediumCell.accessoryType = .None
        }

        if userDefaults.boolForKey("backgroundIsBlack") {
            blackBackgroundCell.accessoryType = .Checkmark
        } else {
            whiteBackgroundCell.accessoryType = .Checkmark
        }
        
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
         let userDefaults = NSUserDefaults.standardUserDefaults()
        if indexPath.section == 1 {
        switch indexPath.row {
        case 0 :
            smallCell.accessoryType = .Checkmark
            mediumCell.accessoryType = .None
            largeCell.accessoryType = .None
            userDefaults.setFloat(13.0, forKey: "fontSize")
            
        case 1:
            smallCell.accessoryType = .None
            mediumCell.accessoryType = .Checkmark
            largeCell.accessoryType = .None
            userDefaults.setFloat(17.0, forKey: "fontSize")
            
        case 2:
            smallCell.accessoryType = .None
            mediumCell.accessoryType = .None
            largeCell.accessoryType = .Checkmark
            userDefaults.setFloat(23.0, forKey: "fontSize")
            
        default:
            smallCell.accessoryType = .None
            mediumCell.accessoryType = .Checkmark
            largeCell.accessoryType = .None
          userDefaults.setFloat(17.0, forKey: "fontSize")
            
        }
        } else if indexPath.section == 0 {
            if indexPath.row == 0 {
                userDefaults.setBool(true, forKey: "backgroundIsBlack")
                blackBackgroundCell.accessoryType = .Checkmark
                whiteBackgroundCell.accessoryType = .None

            } else {
                userDefaults.setBool(false, forKey: "backgroundIsBlack")
                blackBackgroundCell.accessoryType = .None
                whiteBackgroundCell.accessoryType = .Checkmark

            }

        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
//        if let dest = segue.destinationViewController as? UINavigationController {
//            if let destSettingsController = dest.topViewController as? settingsControllerTableViewController {
//                destSettingsController.mySettings = self.mySettings
//                destSettingsController.tableView.reloadData()
//            }
//        }

    }


}
