//
//  UserUtilities.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/11/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import Foundation

enum UserUtilities {
    
//    static var userAuthToken: String? = {
//        let userDefaults = NSUserDefaults.standardUserDefaults()
//        
//        guard let token = userDefaults.stringForKey("user-auth-token") else {
//            return nil
//        }
//        
//        return token
//    }()
//    
    
    static var userAuthToken: String? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        return userDefaults.stringForKey("token")
    }
    
    static func userLoggedIn() -> Bool {
        
        guard let _ = UserUtilities.userAuthToken else {
            return false
        }
        
        //        print("unwrapped token: \(token)")
        
        return true
    }
    
}