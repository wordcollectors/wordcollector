//
//  createAccountTableViewController.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/8/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import UIKit

class createAccountTableViewController: UITableViewController {
    
    //MARK: Properties
    
    var inputsValidated : Bool = false
    var creationSuccessful : Bool = false
//    var mySettings = Settings()
    
    //MARK: Outlets
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!

    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    //MARK: Actions
    @IBAction func createButtonTapped(sender: UIButton) {
        
//        let tabController : AnyObject! = self.storyboard?.instantiateViewControllerWithIdentifier("tabBarViewController")
//    
        validateInputs()
        if inputsValidated {
            createNewAccount()
        }
        
        
    }
    
    func showTabScreen() {
        //        let tabController : AnyObject! = self.storyboard?.instantiateViewControllerWithIdentifier("tabBarViewController")
        //        presentViewController(tabController as! UIViewController, animated: true, completion: nil)
        performSegueWithIdentifier("showTabControllerFromCreate", sender: self)
        
    }
    
    //MARK: methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func validateInputs() {

       
        let userNameText = userNameTextField.text ?? ""
        let passwordText = passwordTextField.text ?? ""
        let passwordTextRepeat = repeatPasswordTextField.text ?? ""
        let userNameOK = userNameText.characters.count > 4
        let passwordOK = passwordText.characters.count >= 10
        let passwordTextRepeatOK = (passwordText == passwordTextRepeat)
        if !passwordTextRepeatOK {
            errorLabel.text = "Passwords do not match."
            print("not OK")
        }
    
        inputsValidated = (userNameOK && passwordOK && passwordTextRepeatOK)
        print("account creation \(inputsValidated)")
        
    }

    func createNewAccount() {
        
        //make a dictionary of the user data
        let firstNameText = firstNameTextField.text ?? ""
        let lastNameText = lastNameTextField.text ?? ""
        let emailText = emailTextField.text ?? ""
        let userNameText = userNameTextField.text ?? ""
        let passwordText = passwordTextField.text ?? ""
        
        let userDataDictionary = ["password": passwordText, "firstName": firstNameText, "email": emailText, "lastName": lastNameText, "userName": userNameText,  "secret": "863a64b140e145c7948a80c8ad80d4acd62729033cabbe891de8dbd14b777603b057dcc347ba5f2c6f6cb474fc1783e048469c8a40bf7f8da1265c9cea8a8bdb"]

        print(userDataDictionary)
        
        //optionals to hold data we will be creating
        var jsonData: NSData?
        var jsonString: NSString?
        var token: String?
        
        //create the JSON data object
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(userDataDictionary, options: [])
        } catch {
            print("Error creating JSON")
        }
        
        //create the request and session objects
        let request = NSMutableURLRequest(URL: NSURL(string: "http://198.150.10.30:8080/fireside/users")!)
        let session = NSURLSession.sharedSession()
        
        //set the headers
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData!
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        // print("4")
        
        //first line of the closure
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            //unwrap and cast the response
            //   print("5")
            guard let testResponse = response as? NSHTTPURLResponse else {
                print("Invalid Response from server: \(response)")
                dispatch_sync(dispatch_get_main_queue()) {
                    self.errorLabel.text = "Unable to create new user"
                }
                return
                
            }
            
            // print("6")
            
            //extract the status code
            
            guard let status = HTTPStatusCodes(rawValue: testResponse.statusCode)else {
                print("A really strange thing happened.")
                dispatch_sync(dispatch_get_main_queue()) {
                    self.errorLabel.text = "Unable to create new user"
                }

                return
            }
            
            //print("7")
            
            //switch on the status
            
            switch status {
            case .Created:
                //print("8")
                let headerFields = testResponse.allHeaderFields
                token = headerFields["user-auth-token"] as? String
                if !(token==nil) {
                    self.creationSuccessful = true
                    print("The token is \(token). Creation Successful = \(self.creationSuccessful)")
                    let userDefaults = NSUserDefaults.standardUserDefaults()
                    userDefaults.setObject(token!, forKey: "token")
                }
                
                guard let returnedData = data else {
                    print("no data was returned")
                    break
                }
                jsonString = NSString(data: returnedData, encoding: NSUTF8StringEncoding) ?? "Invalid JSON"
                print("\(token!)\n\n\(jsonString!)")
                dispatch_sync(dispatch_get_main_queue()) {
                    self.showTabScreen()
                    print(token)
                }
            default:
                print("Unknown status: \(status.rawValue)")
                dispatch_sync(dispatch_get_main_queue()) {
                    self.errorLabel.text = "Unable to create new user"
                }

            }
            
        }
        
        task.resume()
    }
    

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if let dest = segue.destinationViewController as? UINavigationController {
//            if let destSettingsController = dest.topViewController as? settingsControllerTableViewController {
//                destSettingsController.mySettings = self.mySettings
//                destSettingsController.tableView.reloadData()
//            }
//        }
    }
    

}
