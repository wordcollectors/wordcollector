//
//  Message.swift
//  warmHearth
//
//  Created by Huan-Hua Chye on 12/11/15.
//  Copyright © 2015 Huan-Hua Chye. All rights reserved.
//

import Foundation
import CoreData

@objc(Message)
class Message: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    func messageWith(jsonDictionary: [String : AnyObject]) {
        
        messageID = jsonDictionary["id"] as? Int
        userID = jsonDictionary["userID"] as? Int ?? 101
        
        userName = jsonDictionary["userName"] as? String ?? "Unknown"
        userFirstName = jsonDictionary["firstName"] as? String ?? "Unknown"
        userLastName = jsonDictionary["lastName"] as? String ?? "Unknown"
        
        messageBody = jsonDictionary["messageBody"] as? String ?? ""
        messageType = jsonDictionary["public"] as? String ?? "public"
    }
    
}
