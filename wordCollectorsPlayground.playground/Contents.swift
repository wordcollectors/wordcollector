//: Playground - noun: a place where people can play

import UIKit

var str = "hi, you"


func split(messageString: String)->[String] {
    let whitespaceCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
    let components = messageString.componentsSeparatedByCharactersInSet(whitespaceCharacterSet)
    return components
}

func test(message: [String]) -> Bool {
    var longerThanLimit = false
    
    for word in message {
        if word.characters.count > 3 {
            longerThanLimit = true
        }
    }
    return longerThanLimit
    
}

print(split(str))

print(test(split(str)))


let DNU: Character = "A"

func testDNU(message: [String]) -> Bool {
    var usesDNU = false
    
    for word in message {
        for char in word.characters {
            if char == DNU {
                usesDNU = true
            }
        }
    }
    return usesDNU
}




