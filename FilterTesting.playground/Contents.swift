//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

func split(messageString: String)->[String] {
    let whitespaceCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
    let components = messageString.componentsSeparatedByCharactersInSet(whitespaceCharacterSet)
    return components
}


let DNU: Character = "A"

func testDNU(message: [String]) -> Bool {
    var usesDNU = false
    
    for word in message {
        for char in word.characters {
            if String(char).localizedCaseInsensitiveContainsString(String(DNU)) {
                usesDNU = true
            }
        }
    }
    return usesDNU
}


print(testDNU(split(str)))


 