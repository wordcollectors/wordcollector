//
//  Message.swift
//  Firehouse
//
//  Created by Justin M Janisch on 12/8/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import Foundation
import CoreData

@objc(Message)
class Message: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    func messageWith(jsonDictionary: [String : AnyObject]) {
        
        messageID = jsonDictionary["id"] as? Int
        userID = jsonDictionary["userID"] as? Int ?? 101
        
        userName = jsonDictionary["userName"] as? String ?? "Unknown"
        userFirstName = jsonDictionary["firstName"] as? String ?? "Unknown"
        userLastName = jsonDictionary["lastName"] as? String ?? "Unknown"
        
        messageBody = jsonDictionary["messageBody"] as? String ?? ""
        messageType = jsonDictionary["public"] as? String ?? "public"
    }
}
