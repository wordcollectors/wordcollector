//
//  loginTableViewController.swift
//  Firehouse
//
//  Created by Justin M Janisch on 10/27/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import UIKit

class loginTableViewController: UITableViewController {

    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var newAccountButton: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func logoutButton(sender: AnyObject) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("user-auth-token")
    }
       
    @IBAction func userTextBox(sender: UITextField) {
        
        let userNameText = userTextField.text
        let passwordText = passwordTextField.text
        
        // Test to see if username & password is valid
        let userNameValid = userNameText!.characters.count >= 5
        let passwordFieldValid = passwordText!.characters.count >= 5
        
        
        if userNameValid && passwordFieldValid {
            loginButton.enabled = true
        } else  {
            loginButton.enabled = false
        }

    }
    
    @IBAction func cancelButton(sender: UIBarButtonItem) {
        
        let isPresentingInMode = presentingViewController is UINavigationController
        
        if isPresentingInMode {
            dismissViewControllerAnimated(true, completion: nil)
        } else {
            navigationController!.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func loginButton(sender: UIButton) {
        
        login()
    }
    
    func login() {
        
        let userData = createLoginRequestBody()
        var jsonData: NSData?
        var jsonString: NSString?
        var token: String?
        
        // Take dictionary and convert to NSDATA object (NSDATA is an object that holds bits)
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(userData, options: [])
        } catch {
            print("Error creating JSON: \(error)")
        }
        
        // Create request and session
        let session = NSURLSession.sharedSession()
        
        let url = NSURL(string: "http://198.150.10.30:8080/fireside/login")
        let request = NSMutableURLRequest(URL: url!)
        
        
        // Configure request & add header values
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData!
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept") 
        
        // Use the session to send the request to the server.  The closure will be called once the dataTaskWithRequest is finished
        // dataTaskWithRequest will run on the background thread.  This process can be used for FTP, File System, HTTP, etc
        let task = session.dataTaskWithRequest(request) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            // Unwrap & cast response (Header)
            guard let testResponse = response as? NSHTTPURLResponse else {
                print("Invalid Response from server: \(response)")
                return
            }
            
            // Get status of response - HTTPStatusCodes is of type NSHTTPURLResponse
            guard let status = HTTPStatusCode(rawValue: testResponse.statusCode) else {
                print("Unknown status code was returned")
                return
            }
            
            switch status {
            case .Created:
                
                // Get header fields & Save token
                let headerFields = testResponse.allHeaderFields
                token = headerFields["user-auth-token"] as? String
                
                // Unwrap and get data (body or payload) from response
                guard let returnedData = data else {
                    print("no data was returned")
                    break
                }
                
                // Convert JSon Data to a String.
                jsonString = NSString(data: returnedData, encoding: NSUTF8StringEncoding) ?? "Invalid JSON"
                print("Login Response: \(jsonString!)")
                
                // 13. Update UI on Main queue (main thread)
                dispatch_sync(dispatch_get_main_queue()) {

                    let userDefaults = NSUserDefaults.standardUserDefaults()

                    userDefaults.setObject(token!, forKey: "user-auth-token")
                        
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            case .Unauthorized:
                print("Unable to login, invalid Username and/or Password")
            default:
                print("Unknown status: \(status.rawValue)")
                print(String(data: data!, encoding: NSUTF8StringEncoding))
            }
            
        }
        
        task.resume()
        
    }
    
    func createLoginRequestBody() -> [String:String] {
        
        let userName = userTextField.text ?? "unknown"
        let password = passwordTextField.text ?? "unknown"
            
        let userNameDictionary = [
            "userName" : userName,
            "password" : password
            ]
            
        return userNameDictionary
        
    }
}
