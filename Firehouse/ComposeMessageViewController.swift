//
//  ComposeMessageViewController.swift
//  Firehouse
//
//  Created by Justin Janisch on 12/12/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import UIKit
import CoreData

class ComposeMessageViewController: UIViewController {
    
    var managedObjectContext: NSManagedObjectContext?
    var messagesDataSource: MessagesDataSource?

    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var newMessageTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        self.newMessageTextView.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func cancelButtonPressed(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func saveNewMessage(sender: UIBarButtonItem) {
        guard let newMessage = newMessageTextView.text else {
            print("Message is empty")
            return
        }
        
        print(newMessage)
        
        // Post new message
        messagesDataSource?.postMessageToServer(newMessage)
        
        // Dismiss View
        self.dismissViewControllerAnimated(true, completion: nil)
    }


    
    

}
