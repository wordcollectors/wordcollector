//
//  MessagesDataSource.swift
//  Fireplace
//
//  Created by Eric Knapp on 11/22/15.
//  Copyright Â© 2015 Eric Knapp. All rights reserved.
//

import UIKit
import CoreData

class MessagesDataSource : NSObject, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    var messageTimer: NSTimer?
    var managedObjectContext: NSManagedObjectContext?
    var tableView: UITableView?
    
    override init() {
        super.init()
        
        messageTimer = NSTimer.scheduledTimerWithTimeInterval(30.0, target: self, selector: "retrieveMessagesFromTimer", userInfo: nil, repeats: true)
    }
    
    var nonLoadedMessageCount: Int = 0 {
        didSet {
            print("the new value is \(nonLoadedMessageCount)")
            
            if nonLoadedMessageCount >= 0 {
                retrieveMessagesAfterID(nonLoadedMessageCount)
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MessageCell", forIndexPath: indexPath)
        
        configureCell(cell, atIndexPath: indexPath)
        
        return cell
    }
    
    // MARK: - Retrieve Message After ID
    
    func retrieveMessagesAfterID(messageID: Int) {
        
        if !UserUtilities.userLoggedIn() {
            print("Per Message DataSource: User is not logged in.")
            return
        }
        
        let request = retrieveMessagesHTTPRequests(messageID)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: handleRetrieveMessages)
        task.resume()
    }
    
    func handleRetrieveMessages(data: NSData?, response: NSURLResponse?, error: NSError?) {
        
        guard case let createUserResponse as NSHTTPURLResponse = response else {
            print("Invalid Response from server: \(response)")
            return
        }
        
        guard let status = HTTPStatusCode(rawValue: createUserResponse.statusCode) else {
            print("A really strange thing happened.")
            return
        }
        
        let jsonArray = extractMessagesWith(status, data: data)
        
        dispatch_sync(dispatch_get_main_queue()) {
            print("json array count: \(jsonArray.count)")
            if jsonArray.count > 0 {
                self.saveJSONArray(jsonArray)
            }
        }
    }
    
    func extractMessagesWith(status: HTTPStatusCode, data: NSData?) -> [[String : AnyObject]] {
        
        switch status {
        case .OK:
            
            guard let returnedData = data else {
                print("no data was returned")
                return []
            }
            
            var jsonArrayOfDictionaries: [[String : AnyObject]]
            do {
                jsonArrayOfDictionaries = try NSJSONSerialization.JSONObjectWithData(returnedData, options: .MutableLeaves) as! [[String : AnyObject]]
                return jsonArrayOfDictionaries
            } catch {
                print("Error")
                return []
            }
            
        case .Created:
            print("New message successfully added")
            return[]
            
        default:
            print("Unknown status: \(status.rawValue)")
            return []
        }
        
    }
    
    func retrieveMessagesHTTPRequests(messageID: Int) -> NSMutableURLRequest {
        
        guard let token = UserUtilities.userAuthToken else {
            fatalError("In retrieveMessagesHTTPRequests and user is not logged in.")
        }
        
        let request =  NSMutableURLRequest(URL: NSURL(string: "http://198.150.10.30:8080/fireside/messages/\(messageID)")!)
        
        request.HTTPMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(token, forHTTPHeaderField: "user-auth-token")
        
        return request
    }
    
    // MARK: - Post
    func createMessagePost(messageDictionary: [String:String]) -> NSMutableURLRequest {
        var jsonData: NSData?
        
        guard let token = UserUtilities.userAuthToken else {
            fatalError("In retrieveMessagesHTTPRequests and user is not logged in.")
        }
        
        // Take dictionary and convert to NSDATA object
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(messageDictionary, options: [])
        } catch {
            print("Error creating JSON: \(error)")
        }
        
        let request =  NSMutableURLRequest(URL: NSURL(string: "http://198.150.10.30:8080/fireside/messages/")!)
        
        request.HTTPMethod = "POST"
        request.HTTPBody = jsonData!
        request.addValue(token, forHTTPHeaderField: "user-auth-token")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request
    }
    
    func postMessageToServer(messageBody: String) {
        if !UserUtilities.userLoggedIn() {
            print("Cannot Post New Message.  User is not logged in.")
            return
        }
        
        let messageDictionary = [
            "messageBody" : messageBody,
            "messageType" : "public"
        ]
        
        let request = createMessagePost(messageDictionary)
        
        let session = NSURLSession.sharedSession()
        
        let task = session.dataTaskWithRequest(request, completionHandler: handlePostResponse)
        
        task.resume()
        
    }
    
    func handlePostResponse(data: NSData?, response: NSURLResponse?, error: NSError?) {
        
        guard case let createPostResponse as NSHTTPURLResponse = response else {
            print("Invalid Response from server: \(response)")
            return
        }
        
        guard let status = HTTPStatusCode(rawValue: createPostResponse.statusCode) else {
            print("A really strange thing happened.")
            return
        }
        
        let jsonArray = extractMessagesWith(status, data: data)
        
        dispatch_sync(dispatch_get_main_queue()) {
            print("Message Post Response Body: \(jsonArray)")
        }
    }
    
    
    // MARK: - Retrieve Messages
    
    func retrieveMessagesFromTimer() {
        
        let maximumMessageID = maximumSavedMessageID()
        
        retrieveMessagesAfterID(maximumMessageID)
        
    }
    
    func retrieveMessages(callback: (() -> Void)?) {
        
        let maximumMessageID = maximumSavedMessageID()
        
        retrieveMessagesAfterID(maximumMessageID)
        
        callback?()
    }
    
    // MARK: - JSON Functions
    
    
    func saveJSONArray(jsonArray: [[String : AnyObject]]) {
        
        for dictionary: [String : AnyObject] in jsonArray {
            let message: Message = NSEntityDescription.insertNewObjectForEntityForName("Message", inManagedObjectContext: managedObjectContext!) as! Message
            
            message.messageWith(dictionary)
        }
        
        // Save the context.
        do {
            try managedObjectContext!.save()
        } catch {
            print("Unresolved error \(error)")
            abort()
        }
    }
    
    func maximumSavedMessageID() -> Int {
        
        let fetchRequest = NSFetchRequest()
        
        let entity = NSEntityDescription.entityForName("Message", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        let maximumIDPredicate = NSPredicate(format: "messageID = max(messageID)", argumentArray: nil)
        fetchRequest.predicate = maximumIDPredicate
        
        var results: [AnyObject]?
        do {
            results = try managedObjectContext?.executeFetchRequest(fetchRequest)
            print("Execute Fetch Request appears to have worked")
        } catch {
            print("Fetch error: \(error)")
            return 0
        }
        
        if results?.count == 0 {
            return 0
        }
        
        let retrievedMessage = results?[0] as! Message
        let maximumID = Int(retrievedMessage.messageID!)
        print("max ID: \(maximumID)")
        
        return maximumID
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest = NSFetchRequest()
        
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entityForName("Message", inManagedObjectContext: self.managedObjectContext!)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        // Edit the sort key as appropriate.
        let sortDescriptor = NSSortDescriptor(key: "messageID", ascending: false)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext!, sectionNameKeyPath: nil, cacheName: "Master")
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            print("Unresolved error \(error)")
            abort()
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController? = nil
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView?.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            tableView?.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            tableView?.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView?.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
            tableView?.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            configureCell((tableView?.cellForRowAtIndexPath(indexPath!)!)!, atIndexPath: indexPath!)
        case .Move:
            tableView?.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            tableView?.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView?.endUpdates()
    }
    
    func configureCell(cell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        
        let messageCell = cell as! MessageTableViewCell
        
        let message: Message = self.fetchedResultsController.objectAtIndexPath(indexPath) as! Message
        
        let cellText = "\(message.messageID ?? 9999): \(message.messageBody ?? "Oops")"
        let fullName = "\(message.userFirstName ?? "") \(message.userLastName ?? "")"
        
        messageCell.messageBodyTextView.text = cellText
        messageCell.fullNameLabel.text = fullName
        messageCell.userNameLabel.text = message.userName
    }
    
    
}














