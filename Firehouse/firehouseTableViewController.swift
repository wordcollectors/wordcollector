//
//  firehouseTableViewController.swift
//  Firehouse
//
//  Created by Justin M Janisch on 10/27/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import UIKit
import CoreData

class firehouseTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var managedObjectContext: NSManagedObjectContext?  // THIS reference will need to be passed to whatever needs it
    
    var messagesDataSource: MessagesDataSource?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(!UserUtilities.userLoggedIn()) {
            performSegueWithIdentifier("openLogin", sender: self)
        }
        
        messagesDataSource = MessagesDataSource()
        messagesDataSource?.managedObjectContext = managedObjectContext
        messagesDataSource?.tableView = tableView
        tableView.dataSource = messagesDataSource!
        
        self.refreshControl?.addTarget(self, action: "refreshMessages:", forControlEvents: UIControlEvents.ValueChanged)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    // MARK: - Table view data source
    
    override func viewWillAppear(animated: Bool) {
        checkFirstLogin()
        
        if(!UserUtilities.userLoggedIn()) {
            performSegueWithIdentifier("openLogin", sender: self)
        }
    }
    
    // This runs after view is drawn
    override func viewDidAppear(animated: Bool) {
        
        
    }
    
    
    func refreshMessages(event: UIControlEvents) {
        print("Someone pulled the refresh button")
        
        // other code to refresh messages
        messagesDataSource?.retrieveMessagesFromTimer()
        self.refreshControl?.endRefreshing() // Need to call end refresh to get refresh to stop.
        
    }

    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showFullMessage" {
            
            if let destinationView = segue.destinationViewController as? FullMessageViewController {
                let cell = sender as! MessageTableViewCell
                destinationView.fullMessage = (cell.messageBodyTextView.text)
            }
        }
        
        if segue.identifier == "composeMessage" {
            
            if let destinationView = segue.destinationViewController as? ComposeMessageViewController {

                destinationView.messagesDataSource = messagesDataSource
                destinationView.managedObjectContext = managedObjectContext
                
            }
        }
    }

    
    func checkFirstLogin() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let isFirstLaunch = userDefaults.boolForKey("isFirstLaunch")
        
        if isFirstLaunch {
            print("It is first launch")
            performSegueWithIdentifier("openLogin", sender: self)
            userDefaults.setBool(false, forKey: "isFirstLaunch")
        } else {
            print("It is NOT first launch")
        }
    }

}
