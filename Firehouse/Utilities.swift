//
//  Utilities.swift
//  TokenTester
//
//  Created by Justin M Janisch on 11/3/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import Foundation


enum UserUtilities {
    
    static var userAuthToken: String? {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        return userDefaults.stringForKey("user-auth-token")
    }
    
    static func userLoggedIn() -> Bool {
        
        guard let _ = UserUtilities.userAuthToken else {
            return false
        }
        
        return true
    }
    
    static func userLogout() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("user-auth-token")
    }
    
}


// if you add a ":" and a type you can go backwards through the enum,
// in this case take the status code and get the response
enum HTTPStatusCode: Int {
    case Continue                     = 100
    case SwitchingProtocols           = 101
    case OK                           = 200
    case Created                      = 201
    case Accepted                     = 202
    case NonAuthoritativeInformation  = 203
    case NoContent                    = 204
    case ResetContent                 = 205
    case PartialContent               = 206
    case MultipleChoices              = 300
    case MovedPermanently             = 301
    case Found                        = 302
    case SeeOther                     = 303
    case NotModified                  = 304
    case UseProxy                     = 305
    case TemporaryRedirect            = 307
    case BadRequest                   = 400
    case Unauthorized                 = 401
    case PaymentRequired              = 402
    case Forbidden                    = 403
    case NotFound                     = 404
    case MethodNotAllowed             = 405
    case NotAcceptable                = 406
    case ProxyAuthenticationRequired  = 407
    case RequestTimeOut               = 408
    case Conflict                     = 409
    case Gone                         = 410
    case LengthRequired               = 411
    case PreconditionFailed           = 412
    case RequestEntityTooLarge        = 413
    case RequestURITooLarge           = 414
    case UnsupportedMediaType         = 415
    case RequestedRangeNotSatisfiable = 416
    case ExpectationFailed            = 417
    case InternalServerError          = 500
    case NotImplemented               = 501
    case BadGateway                   = 502
    case ServiceUnavailable           = 503
    case GatewayTimeOut               = 504
    case HTTPVersionNotSupported      = 505
}