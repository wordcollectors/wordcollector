//
//  FullMessageViewController.swift
//  Firehouse
//
//  Created by Justin Janisch on 12/12/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import UIKit

class FullMessageViewController: UIViewController {


    @IBOutlet weak var fullMessageTextView: UITextView!
    
    var fullMessage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fullMessageTextView.text = fullMessage ?? "No message"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
