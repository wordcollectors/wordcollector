//
//  Message.swift
//  Firehouse
//
//  Created by Justin M Janisch on 12/1/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import Foundation
import CoreData

@objc(Message)
class Message: NSManagedObject {

// Insert code here to add functionality to your managed object subclass

//    func messageWithJSON(dictionary: [String : AnyObject]) {
//        messageID = dictionary["messageID"] as! Int
//        userID = dictionary["userID"] as! Int
//        messageBody = dictionary["messageBody"] as? String
//        messageType = dictionary["messageType"] as? String
//    }
    func messageWith(jsonDictionary: [String : AnyObject]) {
        
        messageID = jsonDictionary["id"] as? Int
        userID = jsonDictionary["userID"] as? Int ?? 101
        
        userName = jsonDictionary["userName"] as? String ?? "Unknown"
        userFirstName = jsonDictionary["firstName"] as? String ?? "Unknown"
        userLastName = jsonDictionary["lastName"] as? String ?? "Unknown"
        
        messageBody = jsonDictionary["messageBody"] as? String ?? ""
        messageType = jsonDictionary["public"] as? String ?? "public"
    }
    
}
