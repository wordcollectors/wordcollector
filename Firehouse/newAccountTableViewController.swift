//
//  newAccountTableViewController.swift
//  Firehouse
//
//  Created by Justin Janisch on 11/3/15.
//  Copyright © 2015 Justin M Janisch. All rights reserved.
//

import UIKit

class newAccountTableViewController: UITableViewController {

    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var newPasswordRepeatField: UITextField!
    @IBOutlet weak var createAccountButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    // MARK - Action Items
    @IBAction func textBoxChanged(sender: UITextField) {
        
        // PUT TEXTBOX VALIDATION HERE THEN ACTIVATE CREATE NEW ACCOUNT BUTTON
        let firstName = firstNameField.text
        let lastName = lastNameField.text
        let email = emailField.text
        let userName = userNameField.text
        let passwordInitial = newPasswordField.text
        let passwordRepeat = newPasswordRepeatField.text
        
        let nameValid = (firstName?.characters.count >= 1 || lastName?.characters.count >= 1)
        let emailValid = email?.characters.count > 1
        let userNameValid = userName?.characters.count >= 5
        let passwordValid = (passwordInitial?.characters.count >= 8 && (passwordInitial == passwordRepeat))
        
        if (nameValid && emailValid && userNameValid && passwordValid) {
            createAccountButton.enabled = true
        } else {
            createAccountButton.enabled = false
        }
        
        
    }
    
    @IBAction func cancelNewAccountCreation(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func createAccountButtonTapped(sender: UIButton) {
        createNewAccount()
    }
    

    func createNewAccount() {

        let userData = createUserDataDictionary()
        var jsonData: NSData?
        var jsonString: NSString?
        var token: String?
        
        // Take dictionary and convert to NSDATA object (NSDATA is an object that holds bits)
        do {
            jsonData = try NSJSONSerialization.dataWithJSONObject(userData, options: [])
        } catch {
            print("Error creating JSON: \(error)")
        }
        
        // Create request and session
        let session = NSURLSession.sharedSession()
        
        let url = NSURL(string: "http://198.150.10.30:8080/fireside/users")
        let request = NSMutableURLRequest(URL: url!)
        
        
        // Configure request
        request.HTTPMethod = "POST"  // Request TYPE (POST, GET, PUT, DELETE, etc.)
        request.HTTPBody = jsonData!
        // ADD HEADER VALUES
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")  // Were sending Json to the server
        request.addValue("application/json", forHTTPHeaderField: "Accept") // Our application is able to accept Json back as a response

        // Use the session to send the request to the server.  The closure will be called once the dataTaskWithRequest is finished
        // dataTaskWithRequest will run on the background thread.  This process can be used for FTP, File System, HTTP, etc
        let task = session.dataTaskWithRequest(request) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            // Unwrap & cast response (Header)
            guard let testResponse = response as? NSHTTPURLResponse else {
                print("Invalid Response from server: \(response)")
                return
            }
            
            // Get status of response - HTTPStatusCodes is of type NSHTTPURLResponse
            guard let status = HTTPStatusCode(rawValue: testResponse.statusCode) else {
                print("Unknown status code was returned")
                return
            }
            
            // Use switch to perform logic based on status returned.
            switch status {
            case .Created:
                
                // 10. get header fields
                let headerFields = testResponse.allHeaderFields
                token = headerFields["user-auth-token"] as? String
                print("Token: \(token)")
                
                // SAVE TOKEN IN USER DEFAULTS
                let userDefaults = NSUserDefaults.standardUserDefaults()
                userDefaults.setObject(token!, forKey: "user-auth-token")
                
                
                // 11.  Unwrap and get data (body or payload) from response
                guard let returnedData = data else {
                    print("no data was returned")
                    break
                }
                
                // 12. Convert JSon Data to a String.  - Should be returned as a string, but needs to be encoded.
                jsonString = NSString(data: returnedData, encoding: NSUTF8StringEncoding) ?? "Invalid JSON"
                print("\(token!)\n\n\(jsonString!)")
                
                // UI CANNOT be updated from background thread.  The following uses the "grand central dispach" library
                // to get to the main thread (event loop).
                // if there are "_" in the function calls it may be a C function.
                // 13. Update UI on Main queue (main thread)
                dispatch_sync(dispatch_get_main_queue()) {
                    //self.returnTextView.text = "\(token!)\n\n\(jsonString!)"
                }
            
            case .BadRequest:
                print("User account could not be created.")
                
            default:
                print("Unknown status: \(status.rawValue)")
            }
            
        }
        
        task.resume()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func createUserDataDictionary() -> [String : String] {
        let firstName = firstNameField.text ?? "unknown"
        let lastName = lastNameField.text ?? "unknown"
        let email = emailField.text ?? "unknown"
        let userName = userNameField.text ?? "unknown"
        let userPassword = newPasswordField.text ?? "unknown"
        let secret = "863a64b140e145c7948a80c8ad80d4acd62729033cabbe891de8dbd14b777603b057dcc347ba5f2c6f6cb474fc1783e048469c8a40bf7f8da1265c9cea8a8bdb"
        
        
        let userNameDictionary = [
            "userName" : userName,
            "firstName" : firstName,
            "lastName" : lastName,
            "email" : email,
            "password" : userPassword,
            "secret" : secret
        ]
        
        return userNameDictionary
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
